

console.log('page load - entered main.js for js-other api');

send_button = document.getElementById("send-button");
send_button.onmouseup = makeRequest;

function makeRequest(){
    console.log("in makerequest func")

    var selindex = document.getElementById('select-server-address').selectedIndex;

    var url_base = document.getElementById('select-server-address').options[selindex].value;
    
    var port_num = document.getElementById('input-port-number').value;
    console.log("port_num: "+port_num);
    
    var action = "GET"; // default
    if (document.getElementById('radio-get').checked){
            action = "GET";
    } else if (document.getElementById('radio-put').checked) {
            action = "PUT";
    } else if (document.getElementById('radio-post').checked) {
        action = "POST";
    } else if (document.getElementById('radio-delete').checked) {
        action = "DELETE";
    }
    
    console.log("action:" + action);
    
    var key = null;
    if (document.getElementById('checkbox-use-key').checked){
        key = document.getElementById('input-key').value;
    }
    //key: movie id
    console.log("key: "+key);
    
    var message_body = null;
    if (document.getElementById('checkbox-use-message').checked){
        // get key value
        message_body = document.getElementById('text-message-body').value;
    }

    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    if (key){
        var url = url_base + ":" + port_num + "/movies/" + key;
    }
    else{
        var url = url_base + ":" + port_num + "/movies/"
    }

    xhr.open(action, url, true); // 2 - associates request attributes with xhr
    
    //set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        var server_resp = xhr.responseText

        var resp = document.getElementById("response-label");
        resp.innerHTML = server_resp;

        var resp_json = JSON.parse(server_resp);
        var ans = document.getElementById("answer-label");
        var ans_str = String(resp_json["title"])+ " belongs to the genres: " +String(resp_json["genres"]);
 
        if(action == "GET"){
            ans.innerHTML = ans_str;
        }
        else{
            ans.innerHTML = "-";
        }
        
    }

    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }


    if (message_body){
        xhr.send(message_body);
    }
    else{
        xhr.send(null);
    }

}