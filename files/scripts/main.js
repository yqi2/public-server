// respond to button click
console.log("Page load happened!")

var submitButton = document.getElementById('bsr-submit-button')
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log("Entered get Form Info!")
    // get text from title, author and story
    var title_text = document.getElementById('title-text').value + ' ';
    // var author_text = document.getElementById('author-text').value1;
    // var story_text = document.getElementById('text-story').value;
    // console.log('title:' + title_text + ' author: ' + author_text + ' story ' + story_text);

    // get checkbox state
    var genres_string = "";

    if (document.getElementById('bsr-radios-1').checked){
        console.log('detected Formal!');
        genres_string += "formal ";
    }

    else  {
        console.log('detected Casual!');
        genres_string += "casual ";
    }


    if (document.getElementById('checkbox-$-value').checked){
        console.log('detected $!');
        genres_string += "$ ";
    }

    if (document.getElementById('checkbox-$$-value').checked) {
        console.log('detected $$!');
        genres_string += "$$ ";
    }

    if (document.getElementById('checkbox-$$$-value').checked) {
        console.log('detected $$$!');
        genres_string += "$$$ ";
    }

    if (document.getElementById('checkbox-$$$$-value').checked) {
        console.log('detected $$$$!');
        genres_string += "$$$$ ";
    }
    // make genre combined string
    console.log('price range: ' + genres_string);

    if (document.getElementById('checkbox-brunch-value').checked){
        console.log('detected brunch!');
        genres_string += "brunch ";
    }

    if (document.getElementById('checkbox-realMeal-value').checked) {
        console.log('detected realMeal!');
        genres_string += "realMeal ";
    }

    if (document.getElementById('checkbox-coffee-value').checked) {
        console.log('detected coffee!');
        genres_string += "coffee ";
    }

    if (document.getElementById('checkbox-bar-value').checked) {
        console.log('detected bar!');
        genres_string += "bar ";
    }

    // make dictionary
    story_dict = {};
    story_dict['title'] = title_text;
    // story_dict['author'] = author_text;
    // story_dict['story'] = story_text;
    story_dict['price'] = genres_string;
    console.log(story_dict);

    displayStory(story_dict);

}

function displayStory(story_dict){
    console.log('entered displayStory!');
    console.log(story_dict);
    // get fields from story and display in label.
    var story_top = document.getElementById('story-top-line');
    story_top.innerHTML = story_dict['title'] + story_dict['price'];

    // var story_body = document.getElementById('story-body');
    // story_body.innerHTML = story_dict['story'];

}